#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 01:47:28 2019

@author: faribahashemi
"""

from flask import Flask#, g
from flask_restplus import Api, Resource, fields
import ast
from process import process
import datetime as dt


p = process()
data = p.clean_data()

flask_app = Flask(__name__)
app = Api(app=flask_app,
          version = "1.0",
          title="Comparison Between Mutual Funds")

name_space = app.namespace("Mutual Funds", description = 'Information of different mutual funds')
        

request_model_compute_interest = app.model("Request for computing interest of one mutual fund", 
                                {'Name':fields.String(required = True,
                                                       description = 'The name of mutual fund',default='test'),
                                    'From': fields.Date(required = True, 
                                                            dt_format='iso8601',
                                                            description = 'The start date of the investment',
                                                            default = dt.datetime.now().strftime('%Y-%m-%d')),
                                    'To': fields.Date(required = True, 
                                                            dt_format='iso8601',                                                                              
                                                            default = dt.datetime.now().strftime('%Y-%m-%d'),
                                                            description = 'The end date of the investment'),
                                    'InitialInvestment': fields.Integer(required = True,
                                                                         default=100000000,
                                                                         description = 'Initial investment value')})
request_model_compare_by_name = app.model("Request for comparing the given mutual funds",
                                {'Names':fields.String(required = True,
                                                       description = 'The names of mutual funds (Comma Seprated)'),
                                 'From': fields.Date(required = True, 
                                                            dt_format='iso8601',
                                                            description = 'The start date of the investment',
                                                            default = dt.datetime.now().strftime('%Y-%m-%d')),
                                'To': fields.Date(required = True, 
                                                            dt_format='iso8601',                                                                              
                                                            default = dt.datetime.now().strftime('%Y-%m-%d'),
                                                            description = 'The end date of the investment'),
                                'InitialInvestment': fields.Integer(required = True,
                                                                     default=100000000,
                                                                     description = 'Initial investment value')})
                                
filter_fields = app.model("",{'Type':fields.List(fields.String(description='The object type', enum=list(data['نوع / اندازه صندوق'].unique()))),
                              'Guarantee':fields.String(description='The guarantee status', enum=['Yes','No'])})                                                            

request_model_compare_by_type = app.model("Request for comparing mutual funds based on their type and guarantee status",
                                {'Filter': fields.Nested(filter_fields),
                                 'From': fields.Date(required = True, 
                                                            dt_format='iso8601',
                                                            description = 'The start date of the investment',
                                                            default = dt.datetime.now().strftime('%Y-%m-%d')),
                                'To': fields.Date(required = True, 
                                                            dt_format='iso8601',                                                                              
                                                            default = dt.datetime.now().strftime('%Y-%m-%d'),
                                                            description = 'The end date of the investment'),
                                'InitialInvestment': fields.Integer(required = True,
                                                                     default=100000000,
                                                                     description = 'Initial investment value')})                                


@name_space.route("/<string:name>")
class main_class(Resource):
    @app.doc(responses={ 200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error' }, 
             params={ 'name': 'Specify the name associated with the mutual fund' })
    def get(self, name):
        try:
            fund_data = data[data['نام صندوق'] == name]
            print("nav:{} ".format(sorted(ast.literal_eval(fund_data.iloc[0]["nav_unit"]))))
            return fund_data[fund_data.columns[:-1]].to_dict('r')
        except KeyError as e:
            name_space.abort(500, e.__doc__, status = "Could not retrieve information", statusCode = "500")
        except Exception as e:
            name_space.abort(400, e.__doc__, status = "Could not retrieve information", statusCode = "400")

@name_space.route("/all")
class get_all_names(Resource):
    @app.doc(responses={ 200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error' })
    def get(self):
        return list(data['نام صندوق'])  

@name_space.route("/computeInterest")
class compute_interest(Resource):
    @app.expect(request_model_compute_interest)
    def post(self):
        try:
            info = app.payload 
            fund_data = data[data['نام صندوق'] == info['Name']].iloc[0]
            fund_data["nav_unit"] = ast.literal_eval(fund_data["nav_unit"])
            p = process()
            start_timestamp = p.convert_date_to_timestamp(info['From'])
            end_timestamp = p.convert_date_to_timestamp(info['To'])
            initial_investment = info['InitialInvestment']
            return p.compute_interest(fund_data, info['Name'], start_timestamp, end_timestamp, initial_investment)
        except IndexError as e:
            return {"Message": "The mutual fund was not found!"}
        except Exception as e:
            return str(e), 400
            
@name_space.route("/compareByName")
class compare_mutual_funds_by_name(Resource):
    @app.expect(request_model_compare_by_name)
    def post(self):
        result ={'TimeInterval':{}, 'BestMutualFund':{}, 'Details':{}}
        try:
            info = app.payload
            start_timestamp = p.convert_date_to_timestamp(info['From'])
            end_timestamp = p.convert_date_to_timestamp(info['To'])
            
            if start_timestamp > end_timestamp:
                result['Details'] = "The start date could not be greater than the end date!"
                return result
                
            initial_investment = info['InitialInvestment']
            names = info['Names'].split(",")
            for name in names:
                try:
                    fund_data = data[data['نام صندوق'] == name].iloc[0]
                    fund_data["nav_unit"] = ast.literal_eval(fund_data["nav_unit"])
                    fund_dates = sorted(list(fund_data['nav_unit'].keys()))
                    if start_timestamp < fund_dates[0]:
                        start_timestamp = fund_dates[0]
                    if end_timestamp > fund_dates[-1]:
                        end_timestamp = fund_dates[-1]
                except:
                    pass
            result['TimeInterval']={'From':p.convert_timestamp_to_date(start_timestamp).strftime('%Y-%m-%d'),
                                    'To':p.convert_timestamp_to_date(end_timestamp).strftime('%Y-%m-%d')}
            for name in names:
                try:
                    fund_data = data[data['نام صندوق'] == name].iloc[0]
                    fund_data["nav_unit"] = ast.literal_eval(fund_data["nav_unit"])
                    result['Details'][name] = p.compute_interest(fund_data, name, start_timestamp, end_timestamp, initial_investment)
                except IndexError as e:
                    result['Details'][name] = {"Message": "The mutual fund was not found!"}
            result['BestMutualFund']=sorted(result['Details'].items(), key=lambda x: x[1].get('total_iterest_percentage (%)',-1), reverse=True)[0]
            return result

        except Exception as e:
            return str(e), 400
            
@name_space.route("/compareByType")
class compare_mutual_funds_by_type(Resource):
    @app.expect(request_model_compare_by_type)
    def post(self):
        result ={'TimeInterval':{}, 'BestMutualFund':{}, 'Details':{}}
        try:
            info = app.payload
            start_timestamp = p.convert_date_to_timestamp(info['From'])
            end_timestamp = p.convert_date_to_timestamp(info['To'])
            
            if start_timestamp > end_timestamp:
                result['Details'] = "The start date could not be greater than the end date!"
                return result            
            
            initial_investment = info['InitialInvestment']
            
            if info['Filter']['Guarantee'] == "Yes":
                names = list(data.loc[(data['ضامن نقد شوندگی'] != 'None') & (data['نوع / اندازه صندوق'].isin(info['Filter']['Type'])),'نام صندوق'])
            else:
                names = list(data.loc[data['نوع / اندازه صندوق'].isin(info['Filter']['Type']),'نام صندوق'])
            result['TimeInterval']={'From':p.convert_timestamp_to_date(start_timestamp).strftime('%Y-%m-%d'),
                                    'To':p.convert_timestamp_to_date(end_timestamp).strftime('%Y-%m-%d')}
                
            for name in names:
                try:
                    fund_data = data[data['نام صندوق'] == name].iloc[0]
                    fund_data["nav_unit"] = ast.literal_eval(fund_data["nav_unit"])
                    result['Details'][name] = p.compute_interest(fund_data, name, start_timestamp, end_timestamp, initial_investment)
                except IndexError as e:
                    result['Details'][name] = {"Message": "The mutual fund was not found!"}
            sorted_result = sorted(result['Details'].items(), key=lambda x: x[1].get('total_iterest_percentage (%)',-1), reverse=True)
            result['BestMutualFund'] = sorted_result[0]
            result['Details'] = sorted_result
            return result

        except Exception as e:
            return str(e), 400

