#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 26 01:29:19 2019

@author: faribahashemi
"""

import pandas as pd
import ast
import time
import datetime as dt


class process():
    
    
    def clean_data(self):
        not_allowed_list = ['ندارد','ضامن','نامشخص نامشخص','ندارد ? ندارد','ندارد ندارد','ندارد -','این نوع صندوق ندارد.','نامشخص','نا مشخص','ضامن نقد شوندگي']
        data = pd.read_csv("mutual_fund.csv")

        data.loc[data['ضامن نقد شوندگی'].isin(not_allowed_list),'ضامن نقد شوندگی'] = "None"
        data['ضامن نقد شوندگی']=data['ضامن نقد شوندگی'].fillna("None")

#       Replace the values in the guarantee columns with 'None' for funds that do not have any guarantee

        return data

    def convert_date_to_timestamp(self, str_date):
        return int(time.mktime(dt.datetime.strptime(str_date, "%Y-%m-%d").timetuple()))
    
    def convert_timestamp_to_date(self, str_timestamp):
        return dt.date.fromtimestamp(str_timestamp)
        
        
    def calculate_units(self, fund_data, start_timestamp, initial_investment):
        fund_dates = list(fund_data['nav_unit'].keys())
        if start_timestamp >= sorted(fund_dates)[0]:
            NAV_start_date = fund_data['nav_unit'][start_timestamp]['NAV']
            return initial_investment//NAV_start_date
        else:
            raise Exception("The started date of {0} mutual fund is {1} which is after the given date {2}".format(fund_data['نام صندوق'], self.convert_timestamp_to_date(sorted(fund_dates)[0]), self.convert_timestamp_to_date(start_timestamp)))
            
    def calculate_interest_per_unit(self, fund_data, start_timestamp, end_timestamp):# = convert_date_to_timestamp(dt.datetime.today().strftime('%d-%m-%Y'))):
    
        fund_dates = list(fund_data['nav_unit'].keys())
        if (end_timestamp > sorted(fund_dates)[-1]) or (end_timestamp < sorted(fund_dates)[0]):
            raise Exception("The given end date is not valid.")
        if start_timestamp >= sorted(fund_dates)[0]:
            try:
                NAV_end_date = fund_data['nav_unit'][end_timestamp]['NAV']
                NAV_start_date = fund_data['nav_unit'][start_timestamp]['NAV']
                return (NAV_start_date, NAV_end_date, NAV_end_date - NAV_start_date)
            except Exception as e:
                raise Exception("The data for the required date is not provided for this mutual fund")
        else:
            raise Exception("The started date of {0} mutual fund is {1} which is after the given date {2}".format(fund_data['نام صندوق'], self.convert_timestamp_to_date(sorted(fund_dates)[0]), self.convert_timestamp_to_date(start_timestamp)))
        
    def compute_interest(self, fund_data, name, start_timestamp, end_timestamp, initial_investment):
        try:
            (NAV_start_date, NAV_end_date, interest_per_unit) = self.calculate_interest_per_unit(fund_data, start_timestamp, end_timestamp)
            units = self.calculate_units(fund_data, start_timestamp, initial_investment)
            total_interest = units * interest_per_unit
            if units != 0:
                total_iterest_percentage = ((NAV_end_date - NAV_start_date)/NAV_start_date)*100
            else:
                total_iterest_percentage = 0
                interest_per_unit = 0
            validated_initial_investment = units * NAV_start_date
            total_value = validated_initial_investment + total_interest
            return{"total_value": total_value, "total_interest": total_interest, "total_iterest_percentage (%)": round(total_iterest_percentage,2), "validated_initial_investment": validated_initial_investment, "interest_per_unit": interest_per_unit, "units": units,"type": fund_data['نوع / اندازه صندوق'],"guarantee":fund_data['ضامن نقد شوندگی']}
        except Exception as e:
            return {"Message":str(e)}
        
