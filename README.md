# Finding the best mutual find using Python and Flask
### To run analytics part:
* Open terminal and run jupyter notebook
    ```$ jupyter notebook```

### To run RESTful API:
* Open terminal and write the following commands:
    * ```export FLASK_APP=mutual_funds_performance.py```
    * ```python3.6 -m flask run```
    
    The Restful API will be run on: http://127.0.0.1:5000/
